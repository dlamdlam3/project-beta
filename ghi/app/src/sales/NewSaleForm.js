import React, { useEffect, useState } from 'react';

function NewSaleForm() {
  const [vin, setVin] = useState('');
  const [vins, setVins] = useState([]);
  const [salesperson, setSalesperson] = useState('');
  const [salespersons, setSalespersons] = useState([]);
  const [customer, setCustomer] = useState('');
  const [customers, setCustomers] = useState([]);
  const [price, setPrice] = useState('');

  const fetchData = async () => {
    const automobilesURL = 'http://localhost:8100/api/automobiles/';
    const salespersonsURL = 'http://localhost:8090/api/salespeople/';
    const customersURL = 'http://localhost:8090/api/customers/';

    const automobilesResponse = await fetch(automobilesURL);
    if (automobilesResponse.ok) {
      const automobilesData = await automobilesResponse.json();
      setVins(automobilesData.autos);
      console.log(automobilesData);
    }

    const salespersonsResponse = await fetch(salespersonsURL);
    if (salespersonsResponse.ok) {
      const salespersonsData = await salespersonsResponse.json();
      setSalespersons(salespersonsData.salespersons);
      console.log(salespersonsData);
    }

    const customersResponse = await fetch(customersURL);
    if (customersResponse.ok) {
      const customersData = await customersResponse.json();
      setCustomers(customersData.customers);
      console.log(customersData);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.vin = vin;
    data.salesperson = salesperson;
    data.customer = customer;
    data.price = price;

    const salesURL = 'http://localhost:8090/api/sales/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const saleResponse = await fetch(salesURL, fetchOptions);
    if (saleResponse.ok) {
      setVin('');
      setSalesperson('');
      setCustomer('');
      setPrice('');
    }
  };

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select onChange={handleVinChange} value={vin} required name="vin" id="vin" className="form-select">
                <option value="">Select a VIN</option>
                {vins.map((vinOption) => (
                  <option key={vinOption.vin} value={vinOption.vin}>
                    {vinOption.vin}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleSalespersonChange} value={salesperson} required name="employee_id" id="employee_id" className="form-select">
                <option value="">Select Salesperson</option>
                {salespersons.map((salesperson) => (
                  <option key={salesperson.employee_id} value={salesperson.employee_id}>
                    {`${salesperson.first_name} ${salesperson.last_name}`}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                <option value="">Select Customer</option>
                {customers.map((customer) => (
                  <option key={customer.id} value={customer.id}>
                    {`${customer.first_name} ${customer.last_name}`}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <input type="number" value={price} onChange={handlePriceChange} placeholder="Price" className="form-control" />
            </div>
            <button type="submit" className="btn btn-primary">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default NewSaleForm;
