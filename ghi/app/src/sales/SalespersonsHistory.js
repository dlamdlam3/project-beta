import React, { useState, useEffect } from 'react';

const SalespersonHistory = () => {
  const [salespersons, setSalespersons] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [salesData, setSalesData] = useState([]);

  useEffect(() => {
    const fetchSalespersons = async () => {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalespersons(data.salespersons);
      }
    };

    fetchSalespersons();
  }, []);

  useEffect(() => {
    const fetchSalesData = async () => {
      if (selectedSalesperson) {
        const response = await fetch(`http://localhost:8090/api/sales/?salesperson=${selectedSalesperson}`);
        if (response.ok) {
          const data = await response.json();
          setSalesData(data.sales);
        }
      } else {
        setSalesData([]);
      }
    };

    fetchSalesData();
  }, [selectedSalesperson]);

  const handleSalespersonChange = (event) => {
    setSelectedSalesperson(event.target.value);
  };

  return (
    <div>
      <h1 className="mb-0">Salesperson History</h1>
      <div className="w-100 mt-0">
        <select className="form-select" value={selectedSalesperson} onChange={handleSalespersonChange}>
          <option value="">Select a Salesperson</option>
          {salespersons.map((salesperson) => (
            <option key={salesperson.employee_id} value={salesperson.employee_id}>
              {`${salesperson.first_name} ${salesperson.last_name}`}
            </option>
          ))}
        </select>
      </div>

      <div className="shadow p-4 mt-4">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {salesData.map((sale) => (
              <tr key={sale.id}>
                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default SalespersonHistory;
