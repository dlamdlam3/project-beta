import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersPage from './inventory/ManufacturersPage';
import ManufacturersForm from './inventory/ManufacturersForm';
import ModelsPage from './inventory/ModelsPage'
import ModelsForm from './inventory/ModelsForm'
import AutomobilesPage from './inventory/AutomobilesPage'
import AutomobilesForm from './inventory/AutomobilesForm'
import TechniciansPage from './services/TechniciansPage';
import TechniciansForm from './services/TechniciansForm';
import AppointmentsPage from './services/AppointmentsPage';
import AppointmentsForm from './services/AppointmentsForm';
import ServiceHistory from './services/ServiceHistory';
import SalespersonList from './sales/SalespersonList';
import NewSalespersonForm from './sales/NewSalespersonForm';
import CustomerList from './sales/CustomerList';
import NewCustomerForm from './sales/NewCustomerForm';
import SaleList from './sales/SalesList';
import NewSaleForm from './sales/NewSaleForm';
import SalespersonsHistory from './sales/SalespersonsHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturersPage />} />
          <Route path="/manufacturers/create" element={<ManufacturersForm />} />
          <Route path="/models" element={<ModelsPage />} />
          <Route path="/models/create" element={<ModelsForm />} />
          <Route path="/automobiles" element={<AutomobilesPage />} />
          <Route path="/automobiles/create" element={<AutomobilesForm />} />
          <Route path="/technicians" element={<TechniciansPage/>} />
          <Route path="/technicians/create" element={<TechniciansForm/>} />
          <Route path="/appointments/" element={<AppointmentsPage/>} />
          <Route path="/appointments/create" element={<AppointmentsForm/>} />
          <Route path="/appointments/history" element={<ServiceHistory/>} />
          <Route path="/salespeople/" element={<SalespersonList />} />
          <Route path="/salespeople/new/" element={<NewSalespersonForm />} />
          <Route path="/customers/" element={<CustomerList />} />
          <Route path="/customers/new/" element={<NewCustomerForm />} />
          <Route path="/sales/" element={<SaleList />} />
          <Route path="/sales/new/" element={<NewSaleForm />} />
          <Route path="/sales/history/" element={<SalespersonsHistory />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
