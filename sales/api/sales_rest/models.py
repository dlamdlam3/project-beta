from django.db import models
from django.urls import reverse


# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=255, unique=True)
    sold = models.BooleanField(default=False)
    

class Salesperson(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    employee_id = models.CharField(max_length=255, unique=True)
    
    
class Customer(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=20)
    
    
class Sale(models.Model):
    price = models.PositiveIntegerField()
    
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    
    def get_api_url(self):
        return reverse("api_show_sale", kwargs={"pk": self.pk})
