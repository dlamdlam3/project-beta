# CarCar

Team:

* Derrick - Service
* Joesian - Sales

## Design

## Service microservice

Services tracks all technicians and adds new technicians. It tracks all appointments, can create new appointments and you can update the appointment when finished
to store it in a history page. any automobile sold will be tracked as a VIP by checking its VIN.

## Sales microservice

Sales uses a Sale, Customer, Salesperson and an AutomobileVO to keep track of all objects needed in a sale. We can create and list salespeople, customers and sales as well as show all sales associtated with a specific salesperson. If a car has been sold the sale will not be created.
